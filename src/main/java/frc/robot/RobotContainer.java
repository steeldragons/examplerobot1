/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import java.sql.Driver;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.XboxController;
import frc.robot.commands.ExampleCommand;
import frc.robot.commands.LimelightCommand;
import frc.robot.commands.LimelightLessStupid;
import frc.robot.commands.SprintCommand;
import frc.robot.commands.TeleopDrive;
import frc.robot.commands.TryAndHitTheWall;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.ExampleSubsystem;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;

/**
 * This class is where the bulk of the robot should be declared.  Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls).  Instead, the structure of the robot
 * (including subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
  // The robot's subsystems and commands are defined here...
  private final ExampleSubsystem m_exampleSubsystem = new ExampleSubsystem();

  // the DriveTrain <: SubsystemBase subsystem
  private static final DriveTrain driveTrain = new DriveTrain();

  // joystick objects
  public static final Joystick driver = new Joystick(3);

  public static final DigitalInput microswitchInput1 = new DigitalInput(Constants.MICROSWITCH_1_PIN);

  private final ExampleCommand m_autoCommand = new ExampleCommand(m_exampleSubsystem);



  /**
   * The container for the robot.  Contains subsystems, OI devices, and commands.
   */
  public RobotContainer() {
    // Configure the button bindings
    configureButtonBindings();

    // Make sure that the driveTrain subsystem is always running the TeleopDrive command
    // RobotContainer.driveTrain.setDefaultCommand(new TeleopDrive(RobotContainer.driveTrain));
    // RobotContainer.driveTrain.setDefaultCommand(
    //   new LimelightCommand(driveTrain)
    // );
    /// this si icommentedde  toutpu pout for good reason
    ///. we meed to not havve monopoy by TeleopDrirve
    /// anti trusttet law
    /// its tjhe govenremrnemt again
    RobotContainer.driveTrain.setDefaultCommand(
      new TeleopDrive(RobotContainer.driveTrain)
     );
  }

  /**
   * Use this method to define your button->command mappings.  Buttons can be created by
   * instantiating a {@link GenericHID} or one of its subclasses ({@link
   * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a
   * {@link edu.wpi.first.wpilibj2.command.button.JoystickButton}.
   */
  private void configureButtonBindings() {
    //(new JoystickButton(driver, Constants.K_BUTTON_SPRINT))
    //  .whenPressed(new SprintCommand(RobotContainer.driveTrain));
    (new JoystickButton(driver, Constants.K_BUTTON_SPRINT))
     .whenHeld(new LimelightCommand(driveTrain));
    (new JoystickButton(driver, Constants.K_BUTTON_TRYANDHITTHEWALL))
      .whenPressed(new TryAndHitTheWall(RobotContainer.driveTrain));
  }


  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  public Command getAutonomousCommand() {
    // An ExampleCommand will run in autonomous
    return m_autoCommand;
  }

  public static double[] limeadeIsUnderrated() {
    double[] vals = new double[2];
    NetworkTable table = NetworkTableInstance.getDefault().getTable("limelight-dragons");
    NetworkTableEntry tx_recv = table.getEntry("tx");
    NetworkTableEntry ty_recv = table.getEntry("ty");

    vals[0] = tx_recv.getDouble(0.0);
    vals[1] = ty_recv.getDouble(0.0);

    return vals;
  }
}
