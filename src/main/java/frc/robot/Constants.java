/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants.  This class should not be used for any other purpose.  All constants should be
 * declared globally (i.e. public static).  Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {
    public static final double DRIVE_TRAIN_THROTTLE_SCALAR = 0.8;
    public static final double DRIVE_TRAIN_STEERING_SCALAR = 0.8;

    public static final int LEFT_JOYSTICK_X_AXIS = 0;
    public static final int LEFT_JOYSTICK_Y_AXIS = 1;

    public static final double SPRINT_DURATION = 0.2;
    public static final int K_BUTTON_SPRINT = 1;

    public static final double TURN_AROUND_TIME = 1;

    public static final double TRYANDHITTHEWALL_TIMEOUT_DURATION = 5;
    public static final int K_BUTTON_TRYANDHITTHEWALL = 2;

    public static final int MICROSWITCH_1_PIN = 8;
}
