/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import frc.robot.RobotContainer;
import frc.robot.subsystems.DriveTrain;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;

/**
 * An example command that uses an example subsystem.
 */
public class LimelightCommand extends CommandBase {
  @SuppressWarnings({"PMD.UnusedPrivateField", "PMD.SingularField"})

  public DriveTrain drive;
  public double startTime;

  public final double time_duration = 0.5;

  /**
   * Creates a new ExampleCommand.
   *
   * @param drive The subsystem used by this command.
   */
  public LimelightCommand(DriveTrain drive) {
    this.drive = drive;
    addRequirements(drive);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    this.startTime = Timer.getFPGATimestamp();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if ((Timer.getFPGATimestamp()-startTime) > (this.time_duration/2)) {
      double scale_factor = 2.0;
      double x = RobotContainer.limeadeIsUnderrated()[0];
      drive.arcadeDrive(-0.7, ((x/50)*scale_factor));
    } else {
      double x = RobotContainer.limeadeIsUnderrated()[0];
      double scale_factor = 2.0;
      double steering = ((x/50)*scale_factor);
      System.out.println(steering);
      drive.arcadeDrive(0.7, steering);
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    drive.stop();
    System.out.println("IM DYING MY LAST WORDS ARE FUCK ALL OF YOU");
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    // double time_duration = 0.75;
    if ((Timer.getFPGATimestamp()-startTime) > this.time_duration) {
      this.startTime = Timer.getFPGATimestamp();
    }
    return false;
  }
}
