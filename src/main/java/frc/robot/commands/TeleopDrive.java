/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import frc.robot.Constants;
import frc.robot.RobotContainer;
import frc.robot.subsystems.DriveTrain;

import edu.wpi.first.wpilibj2.command.CommandBase;

/**
 * the TeleopDrive <: CommandBase command
 */
public class TeleopDrive extends CommandBase {
  @SuppressWarnings({"PMD.UnusedPrivateField", "PMD.SingularField"})
  private final DriveTrain driveTrain;

  /**
   * Creates a new ExampleCommand.
   *
   * @param driveTrain The subsystem used by this command.
   */
  public TeleopDrive(DriveTrain driveTrain) {
    this.driveTrain = driveTrain;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(driveTrain);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
      double steering, throttle;
      if (RobotContainer.microswitchInput1.get()) {
        throttle = 0.0;
        steering = 0.0;
      } else {
        throttle =
          Constants.DRIVE_TRAIN_THROTTLE_SCALAR * RobotContainer.driver.getRawAxis(Constants.LEFT_JOYSTICK_Y_AXIS);
        steering =
        Constants.DRIVE_TRAIN_STEERING_SCALAR * RobotContainer.driver.getRawAxis(Constants.LEFT_JOYSTICK_X_AXIS);
      }
      driveTrain.arcadeDrive(throttle, steering);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
