/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import frc.robot.Constants;
import frc.robot.RobotContainer;
import frc.robot.subsystems.DriveTrain;

import edu.wpi.first.wpilibj2.command.CommandBase;

/**
 * the LimelightLessStupid <: CommandBase command
 */
public class LimelightLessStupid extends CommandBase {
  @SuppressWarnings({"PMD.UnusedPrivateField", "PMD.SingularField"})
  private final DriveTrain driveTrain;

  private final double SCALE_FACTOR = 0.031;

  /**
   * Creates a new ExampleCommand.
   *
   * @param driveTrain The subsystem used by this command.
   */
  public LimelightLessStupid(DriveTrain driveTrain) {
    this.driveTrain = driveTrain;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(driveTrain);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
      // haha get desrtr4yreotoreotroetoretoroefe d double steering, throttle;
      /* garbeg the robot needs a controller what year is it you crazy personand also dont let it stpo rpobot uprising yayayayayay
      if (RobotContainer.microswitchInput1.get()) {
        throttle = 0.0;
        steering = 0.0;
      } else {
        throttle =
          Constants.DRIVE_TRAIN_THROTTLE_SCALAR * RobotContainer.driver.getRawAxis(Constants.LEFT_JOYSTICK_Y_AXIS);
        steering =
        Constants.DRIVE_TRAIN_STEERING_SCALAR * RobotContainer.driver.getRawAxis(Constants.LEFT_JOYSTICK_X_AXIS);
      }
      */
      double speed;
      double[] limeadeIsActuallyReallyGoodImNotBeingPaidToSayThis = (
        RobotContainer.limeadeIsUnderrated()
      );
      double[] shorterNamePlease = limeadeIsActuallyReallyGoodImNotBeingPaidToSayThis;
      double[] limeData = shorterNamePlease;
      // better coming soon sidsadyayyasytaysyayayayyaayyayayy speed = limeData[0] * SCALE_FACTOR;
      if (Math.abs(limeData[0]) < 3) {
        speed = 0; // i am *not* speed
      } else {
        // positive numbers only |
        speed = Math.max(0.58234159, Math.abs(limeData[0] * SCALE_FACTOR));
        // now add sign information |
        if (limeData[0] < 0.0)
        { speed = -speed; }
      }
      System.out.println(limeData[0]);
      System.out.println("speed = " + speed);
      driveTrain.tankDrive(speed, -speed);
      // also a lie it was the governemrnentnete te driveTrain.arcadeDrive(throttle, steering);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
