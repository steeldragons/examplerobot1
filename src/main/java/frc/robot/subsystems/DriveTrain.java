/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.Spark;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class DriveTrain extends SubsystemBase {
  private final DifferentialDrive differentialDrive;
  
    /**
   * Creates a new DriveTrain <: SubsystemBase.
   */
  public DriveTrain() {
    // setup/instantiate Spark motor controller objects
    Spark left1 = new Spark(0);
    Spark left2 = new Spark(1);
    Spark right1 = new Spark(2);
    Spark right2 = new Spark(3);

    // combine motors that are on the same side and need to act as one unit with SpeedControllerGroup
    SpeedControllerGroup leftMotors = new SpeedControllerGroup(left1, left2);
    SpeedControllerGroup rightMotors = new SpeedControllerGroup(right1, right2);

    // put everything together into a DifferentialDrive object to control them all easily
    differentialDrive = new DifferentialDrive(leftMotors, rightMotors);
  }

  public void arcadeDrive(double throttle, double steer) {
    differentialDrive.arcadeDrive(throttle, steer);
  }

  public void tankDrive(double leftSpeed, double rightSpeed) {
    differentialDrive.tankDrive(leftSpeed, rightSpeed);
  }

  public void stop() {
    differentialDrive.arcadeDrive(0.0, 0.0);
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}
